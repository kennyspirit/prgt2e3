/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt2e3;

/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-oct-2013
 */

public class Ejercicio0214 {
  public static void main(String[] args) {
    int t=12345,horas,minutos,segundos;
    System.out.println(t);
    horas = t/3600;
    int restohoras = t % 3600;
    minutos = restohoras / 60;
    segundos = restohoras % 60;
    System.out.println("Horas:"+horas+" Minutos:"+minutos+" Segundos:"
    +segundos);
    System.out.println(horas*3600+minutos*60+segundos);
  }
}

/* EJECUCION:
12345
Horas:3 Minutos:25 Segundos:45
12345
*/
